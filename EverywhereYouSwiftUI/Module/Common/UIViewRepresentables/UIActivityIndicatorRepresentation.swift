//
//  UIActivityIndicatorRepresentation.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 26/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct ActivityIndicator: UIViewRepresentable {

    func makeUIView(context: Context) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: .large)
    }

    func updateUIView(_ uiView: UIActivityIndicatorView,
                      context: Context) {
        uiView.startAnimating()
    }
}
