//
//  MapViewRepresentation.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 29/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import SwiftUI
import MapKit
import UIKit

struct MapView: UIViewRepresentable {

    let getWeatherForCoord: ((_ coordinate: Coord) -> Void)
    let removeWeatherForCoord: ((_ coordinate: Coord) -> Void)

    let currentMapCoordinates: [Coord]
    var isLoading: Bool

    @State var addedPoint: CGPoint?
    @State var removeAnnotation: MKAnnotation?

    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.delegate = context.coordinator
        mapView.alpha = 0
        currentMapCoordinates.forEach { (coord) in
            mapView.addAnnotation(getAnnotation(for: coord))
        }

        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: context.coordinator, action: #selector(Coordinator.handleLongPress(sender:)))
        mapView.addGestureRecognizer(longPressGestureRecognizer)

        mapView.showAnnotations(mapView.annotations, animated: true)

        return mapView
    }

    func updateUIView(_ mapView: MKMapView, context: Context) {

        let allAnnotations = mapView.annotations
        mapView.removeAnnotations(allAnnotations)

        currentMapCoordinates.forEach { (coord) in
            mapView.addAnnotation(getAnnotation(for: coord))
        }

        if isLoading == false {

            if let pointToAdd = addedPoint {
                addPlace(to: mapView, for: pointToAdd)
            }

            if let annotationToRemove = removeAnnotation {
                removePlace(for: annotationToRemove)
            }
        }
    }

    func addPlace(to mapView: MKMapView, for point: CGPoint) {
        let locationCoordinate = mapView.convert(point, toCoordinateFrom: mapView)
        let coordinate = Coord(lat: locationCoordinate.latitude, lon: locationCoordinate.longitude)
        let changedAnnotation = getAnnotation(for: coordinate)

        if !annotationIsInDataSource(changedAnnotation) {
            self.getWeatherForCoord(coordinate)
        } else {
            print("COORDINATE IS IN THE DATA SOURCE ALREADY")
        }
    }

    func removePlace(for annotation: MKAnnotation) {
        if annotationIsInDataSource(annotation) {
            self.removeWeatherForCoord(Coord(lat: annotation.coordinate.latitude, lon: annotation.coordinate.longitude))
        } else {
            print("COORDINATE IS ALREADY REMOVED FROM DATASOURCE")
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator($addedPoint, $removeAnnotation)
    }

    func getAnnotation(for coordinate: Coord) -> MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.title = "Hello!"
        annotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.lat, longitude: coordinate.lon)
        return annotation
    }

    func annotationIsInDataSource(_ annotation: MKAnnotation) -> Bool {
        return currentMapCoordinates.contains(Coord(lat: annotation.coordinate.latitude.round(to: 2), lon: annotation.coordinate.longitude.round(to: 2)))
    }

    class Coordinator: NSObject, MKMapViewDelegate {

        @Binding var addedPoint: CGPoint?
        @Binding var removeAnnotation: MKAnnotation?

        private var selectedAnnotation: MKAnnotation?

        init(_ addedPointBinding: Binding<CGPoint?>, _ removeAnnotationBinding: Binding<MKAnnotation?>) {
            _addedPoint = addedPointBinding
            _removeAnnotation = removeAnnotationBinding
        }

        @objc func handleLongPress(sender: UILongPressGestureRecognizer) {
            if sender.state != UIGestureRecognizer.State.began { return }
            self.addedPoint = sender.location(in: sender.view)
        }

        func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
            UIView.animate(withDuration: 0.25) {
                mapView.alpha = 1
            }
        }

        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

            var annotationView: MKAnnotationView?

            let reuseID = "reuseIdentifier"

            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID) {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
            } else {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
                annotationView?.rightCalloutAccessoryView = getDisclosureButton()
            }

            if let annotationView = annotationView {

                if let image = UIImage(systemName: "sun.max.fill") {
                    annotationView.image = image
                }
                annotationView.canShowCallout = true
            }

            return annotationView
        }

        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            if let annotation = view.annotation {
                selectedAnnotation = annotation
            } else {
                assert(true, "ERROR: didSelect an anootation view that has lost the reference to the annotation view")
            }
        }

        func getDisclosureButton() -> UIButton {
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            button.setImage(UIImage(systemName: "minus.circle"), for: .normal)
            button.tintColor = .systemRed
            button.addTarget(self, action: #selector(removeCity), for: .touchUpInside)
            return button
        }

        @objc func removeCity() {
            removeAnnotation = selectedAnnotation
        }
    }

}
