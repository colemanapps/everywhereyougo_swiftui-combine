//
//  Repository.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation

protocol Repository {
    associatedtype RemoteSource: Any
    associatedtype LocalSource: Any

    var remoteDataSource: RemoteSource? { get set }
    var localDataSource: LocalSource! { get set }
    init(remoteDataSource: RemoteSource?, localDataSource: LocalSource)
}
