//
//  Double.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

extension Double {
    func roundToNearestHalf() -> Double {
        let n = 1 / 0.5
        let numberToRound = self * n
        return numberToRound.rounded() / n
    }


    func round(to places: Int) -> Double {
        return Double(Int((pow(10, Double(places)) * self).rounded())) / pow(10, Double(places))
    }
}
