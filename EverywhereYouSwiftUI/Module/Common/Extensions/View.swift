//
//  View.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 28/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import SwiftUI

extension View {
    func eraseToAnyView() -> AnyView {
        return AnyView(self)
    }

}
