//
//  Int.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

extension Int {
    func roundToThree() -> Int{
        let fractionNum = Double(self) / 3.0
        let roundedNum = Int(floor(fractionNum))
        return roundedNum * 3
    }
}
