//
//  Collection.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
