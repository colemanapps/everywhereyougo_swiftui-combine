//
//  Persistable.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import RealmSwift

protocol Persistable {
    associatedtype ManagedObject: RealmSwift.Object
    init(managedObject: ManagedObject)
    var managedObject: ManagedObject { get }
}
