//
//  CityWeatherReportObject.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import RealmSwift

final class CityWeatherReportObject: Object {

    override static func primaryKey() -> String? {
        return "name"
    }

    var weather = List<DescriptiveWeatherObject>()
    @objc dynamic var name: String = ""
    @objc dynamic var coordinate: CoordObject? = CoordObject()

    var model: CityWeatherReport
    {
        get {
            let coord = coordinate ?? CoordObject()
            return CityWeatherReport(weather: weather.compactMap { $0.model }, name: name, coordinate: coord.model)
        }
    }

//    let rain: Rain?
//    let wind: Wind?
//    let main: Main
//    let forecast: [CityWeatherReport]?
//    let fetchTime: Date
//    let dt_txt: String?

}
