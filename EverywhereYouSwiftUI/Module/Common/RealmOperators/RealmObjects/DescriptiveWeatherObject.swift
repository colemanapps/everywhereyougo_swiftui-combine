//
//  DescriptiveWeatherObject.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import RealmSwift

final class DescriptiveWeatherObject: Object  {

    override static func primaryKey() -> String? {
        return "weatherDescription"
    }

    @objc dynamic var weatherDescription: String = ""
    @objc dynamic var icon: String = ""

    var model: DescriptiveWeather
    {
        get {
            return DescriptiveWeather(weatherDescription: weatherDescription, icon: icon)
        }
    }
}

