//
//  CoordObject.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import RealmSwift

class CoordObject: Object {

    override static func primaryKey() -> String? {
        return "identifier"
    }

    enum CodingKeys: CodingKey { case lat, lon }

    @objc dynamic var identifier: String = UUID().uuidString
    @objc dynamic var lat: Double = -2
    @objc dynamic var lon: Double = -2

    var model: Coord
    {
        get {
            return Coord(lat: lat, lon: lon)
        }
    }
}
