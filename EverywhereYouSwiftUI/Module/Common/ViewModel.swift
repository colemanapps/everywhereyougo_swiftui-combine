//
//  ViewModel.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 28/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation

enum ViewState<T> {
    case idle
    case loading(T?)
    case loaded(T)
    case error(Error)
}

protocol ViewStateViewModel {
    associatedtype DataType: Any
    var state: ViewState<DataType> { get }

    var data: DataType? { get set }
}
