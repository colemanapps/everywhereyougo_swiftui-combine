//
//  AddWeatherView.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 29/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import SwiftUI
import MapKit

struct AddWeatherView: View {

    @ObservedObject var viewModel: AddWeatherViewModel
    @Binding var isPresented: Bool

    var body: some View {
        VStack {
            Text("Add or remove locations by pressing the map!").customFont(name: "Avenir Next", style: .title2)
                .padding(24)
                .multilineTextAlignment(.center)
            contentView
            CloseButtonView(isPresented: $isPresented)
        }
        .edgesIgnoringSafeArea(.bottom)
        .background(Color("Orange"))
    }

    private var contentView: some View {
        switch viewModel.state {
        case .idle:
            fatalError("ERROR: This viewmodel should never be idle")
        case .loading(let previousPlaces):
            return mapView(isLoading: true, reports: previousPlaces!).eraseToAnyView()
        case .error(let error):
            return Text(error.localizedDescription).eraseToAnyView()
        case .loaded(let places):
            return mapView(isLoading: false, reports: places).eraseToAnyView()
        }
    }

    func mapView(isLoading: Bool, reports: [CityWeatherReport]) -> some View {
        ZStack(alignment: .center) {
            MapView(getWeatherForCoord: getWeather(_:), removeWeatherForCoord: removeWeather(_:), currentMapCoordinates: reports.map{ $0.coordinate }, isLoading: isLoading)
                .disabled(isLoading)
                .blur(radius: isLoading ? 2 : 0)
                .background(ActivityIndicator().padding())

            if isLoading {
                ActivityIndicator().padding()
            }
        }
        .eraseToAnyView()
    }

    private func getWeather(_ coordinate: Coord) {
        viewModel.addWeatherReport(with: coordinate)
    }

    private func removeWeather(_ coordinate: Coord) {
        viewModel.deleteWeatherReport(with: coordinate)
    }
}

struct AddWeatherView_Previews: PreviewProvider {
    static var previews: some View {
        AddWeatherView(viewModel: AddWeatherViewModel(currentPlaces: []), isPresented: .constant(true))
    }
}
