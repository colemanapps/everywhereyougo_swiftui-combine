//
//  WeatherReportsView.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import SwiftUI

enum ActiveSheet {
   case settings, addView
}

struct WeatherReportsView: View {

    @ObservedObject  private(set) var viewModel = WeatherReportsViewModel()
    @State private var editMode = EditMode.inactive
    @State private var showModal = false
    @State private var activeSheet: ActiveSheet = .settings

    var body: some View {

        NavigationView {
            VStack {
                Spacer()
                contentView
                Spacer()
                FooterButton(viewModel: viewModel, showModal: $showModal, activeSheet: $activeSheet, editMode: editMode)
            }
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading: Button(action: settingsButtonPressed) {

                Image(systemName: "gear")
                    .font(Font.headline.weight(.semibold))
                }, trailing:

                HStack {
                    EditButton()
                    Spacer().frame(width: 20, alignment: Alignment.leading)
                    Button(action: searchButtonPressed) {
                        Image(systemName: "magnifyingglass")
                            .font(Font.headline.weight(.semibold))

                    }
                }
            )
               .environment(\.editMode, $editMode)
            .edgesIgnoringSafeArea(.bottom)
        }
        .sheet(isPresented: self.$showModal, onDismiss: {
            if self.activeSheet == .addView {
                self.viewModel.getAllWeatherReports()
            }
        }) {
            if self.activeSheet == .settings {
                SettingsView(isPresented: self.$showModal)
            } else {
                AddWeatherView(viewModel: AddWeatherViewModel(currentPlaces: self.viewModel.data ?? []), isPresented: self.$showModal)
            }
        }
        .onAppear {
            UITableView.appearance().tableFooterView = UIView()
            self.viewModel.getAllWeatherReports()
        }.onDisappear {
            UITableView.appearance().tableFooterView = nil
        }
    }

    private var contentView: some View {
        switch viewModel.state {
        case .idle:
            return Color.clear.eraseToAnyView()
        case .loading(let previousData):
            return loadingView(previousData).eraseToAnyView()
        case .error(let error):
            return Text(error.localizedDescription).eraseToAnyView()
        case .loaded(let weatherReports):
            return WeatherListView(isLoading: false, viewModel: viewModel, reports: weatherReports).eraseToAnyView()
        }
    }

    func loadingView(_ previouslyLoaded: [CityWeatherReport]?) -> some View {
        if let loaded = previouslyLoaded, !loaded.isEmpty {
            return WeatherListView(isLoading: true, viewModel: viewModel, reports: loaded).eraseToAnyView()
        } else {
            return ActivityIndicator().eraseToAnyView()
        }
    }

    private func settingsButtonPressed() {
        self.activeSheet = .settings
        self.showModal = true
    }

    private func searchButtonPressed() {
        //        viewModel.addWeatherReport(with: Coord(lat: .random(in: 0...100), lon: .random(in: 0...100)))
    }
}

struct WeatherReportView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherReportsView()
    }
}

struct WeatherReportCell: View {

    var report: CityWeatherReport

    var body: some View {
        VStack(alignment: .leading) {
            Text(report.name).customFont(name: "Avenir Next", style: .title3)
            Text(report.weather.first?.weatherDescription.capitalized ?? "").customFont(name: "Avenir Next", style: .subheadline)
        }.padding(.all, 4)
    }
}

struct FooterButton: View {

    @ObservedObject var viewModel: WeatherReportsViewModel
    @Binding var showModal: Bool
    @Binding var activeSheet: ActiveSheet

    var editMode: EditMode

    var body: some View {
        Button(action: addNewLocationPressed) {
            Text(editMode.isEditing ? "Remove all locations" : "Add new location").customFont(name: "Avenir Next", style: .title3)
        }.frame(minWidth: 0, maxWidth: .infinity)
            .padding(EdgeInsets(top: 20, leading: 0, bottom: 50, trailing: 0))
            .background(Color("Orange"))
            .foregroundColor(Color("Black"))
    }

    private func addNewLocationPressed() {
        if editMode == .inactive {
            self.activeSheet = .addView
            self.showModal = true
        } else {
            viewModel.deleteAllWeatherReports()
        }
    }
}

struct WeatherListView: View {

    var isLoading: Bool
    @ObservedObject var viewModel: WeatherReportsViewModel
    var reports: [CityWeatherReport]

    var body: some View {
        return ZStack(alignment: .center) {
            List {
                ForEach(reports) { report in
                    WeatherReportCell(report: report)
                }
                .onDelete(perform: delete)
                .onMove(perform: move)
            }
            .disabled(isLoading)
            .blur(radius: isLoading ? 2 : 0)

            if isLoading {
                ActivityIndicator().padding()
            }
        }
    }

    private func delete(at offsets: IndexSet) {
        viewModel.deleteWeatherReports(at: offsets)
    }

    private func move(source: IndexSet, destination: Int) {
        print("move")
    }
}
