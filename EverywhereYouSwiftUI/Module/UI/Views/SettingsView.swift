//
//  SettingsView.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 28/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import SwiftUI

struct SettingsView: View {

    @Binding var isPresented: Bool
    @ObservedObject private var viewModel = SettingsViewModel()

    var body: some View {

        contentView
        .onAppear(perform: {
            self.viewModel.getUnit()
        })
        .edgesIgnoringSafeArea(.bottom)
        .background(Color("Orange"))
    }

    private var contentView: some View {
        switch viewModel.state {
        case .idle:
            return Color.clear.eraseToAnyView()
        case .loading(let previousData):
            return loadingView(previousData).eraseToAnyView()
        case .error(let error):
            return Text(error.localizedDescription).eraseToAnyView()
        case .loaded(let settings):
            return settingsView(isLoading: false, settings: settings).eraseToAnyView()
        }
    }

    func loadingView(_ previouslyLoaded: Settings?) -> some View {
        if let loadedSettings = previouslyLoaded {
            return settingsView(isLoading: true, settings: loadedSettings).eraseToAnyView()
        } else {
            return ActivityIndicator().eraseToAnyView()
        }
    }

    func settingsView(isLoading: Bool, settings: Settings) -> some View {

        VStack {
            Toggle(isOn: $viewModel.isMetric, label: {
                Text("Use Metric System").customFont(name: "Avenir Next", style: .title3)
                })
                .padding()
            Spacer()

            CloseButtonView(isPresented: $isPresented)
        }
    }

}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(isPresented: .constant(false))
    }
}

struct CloseButtonView: View {

    @Binding var isPresented: Bool

    var body: some View {
        Button(action: {
            self.isPresented = false
        }) {
            Text("Close").customFont(name: "Avenir Next", style: .title3)
        }.frame(minWidth: 0, maxWidth: .infinity)
            .padding(EdgeInsets(top: 20, leading: 0, bottom: 50, trailing: 0))
            .background(Color("White"))
            .foregroundColor(Color("Black"))
    }
}
