//
//  WeatherViewModel.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 01/05/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import Combine

class WeatherViewModel: ViewStateViewModel, ObservableObject {

    typealias DataType = [CityWeatherReport]
    @Published var state: ViewState<[CityWeatherReport]>

    var data: [CityWeatherReport]? {
        didSet {
            self.state = .loaded(data!)
        }
    }
    var subscriptions = Set<AnyCancellable>()
    let weatherRepository: WeatherRepository

    init(weatherRepository: WeatherRepository = WeatherRepository(remoteDataSource: WeatherRemoteDataSource(with: WeatherService()), localDataSource: WeatherLocalDataSource())) {
        self.weatherRepository = weatherRepository
        self.state = .idle
    }

    func deleteWeatherReports(_ reports: [CityWeatherReport]) {

        self.weatherRepository.deleteWeatherReports(reports: reports)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print($0) },
                  receiveValue: { reportArray in
                    self.data = reportArray
            })
            .store(in: &subscriptions)
    }

}

