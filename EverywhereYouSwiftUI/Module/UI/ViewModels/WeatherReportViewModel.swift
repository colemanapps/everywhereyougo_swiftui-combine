//
//  WeatherReportViewModel.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import Combine

final class WeatherReportsViewModel: WeatherViewModel {

    func getAllWeatherReports() {

        DispatchQueue.main.async {
            self.state = .loading(self.data)
        }

        self.weatherRepository.getAllWeatherReports()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print($0) }, receiveValue: { reportArray in
                self.data = reportArray
            })
            .store(in: &subscriptions)
    }


    func deleteAllWeatherReports() {
        if let count = data?.count {
            deleteWeatherReports(at: IndexSet(0..<count))
        }
    }

    func deleteWeatherReports(at offsets: IndexSet) {
        let reportsAtOffset = offsets.compactMap { data?[$0] }
        super.deleteWeatherReports(reportsAtOffset)
    }
}

