//
//  AddWeatherViewModel.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 30/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import Combine
import UIKit

final class AddWeatherViewModel: WeatherViewModel {

    init(currentPlaces: [CityWeatherReport], weatherRepository: WeatherRepository = WeatherRepository(remoteDataSource: WeatherRemoteDataSource(with: WeatherService()), localDataSource: WeatherLocalDataSource())) {

        super.init(weatherRepository: weatherRepository)
        self.state = .loaded(currentPlaces)
        self.data = currentPlaces
    }

    internal func addWeatherReport(with coord: Coord) {

        DispatchQueue.main.async {
            self.state = .loading(self.data)
        }

        weatherRepository.addWeatherReport(with: coord)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print($0) },
                  receiveValue: { report in
                    var places = self.data!
                    places.append(report)
                    self.data = places
            })
            .store(in: &subscriptions)
    }

    func deleteWeatherReport(with coord: Coord) {
        if let reportsWithCoord = data?.filter({ $0.coordinate == coord }) {
            super.deleteWeatherReports(reportsWithCoord)
        } else {
            assert(true, "ERROR: Trying to delete a weather report that is not in the datasource")
        }
    }
}

