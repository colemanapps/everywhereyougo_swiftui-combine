//
//  SettingsViewModel.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 28/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

final class SettingsViewModel: ViewStateViewModel, ObservableObject {

    // TODO: Save the settings in an environment object
    typealias DataType = Settings
    @Published var state: ViewState<Settings>
    @Published var isMetric: Bool = true {
        didSet {
            setUnit(with: isMetric ? Unit.Metric : Unit.Imperial)
        }
    }

    var data: Settings? {
        didSet {
            self.state = .loaded(data!)
        }
    }

    private let settingsRepository: SettingsRepository

    private var subscriptions = Set<AnyCancellable>()

    init(settingsRepository: SettingsRepository = SettingsRepository(localDataSource: SettingsLocalDataSource())) {
        self.settingsRepository = settingsRepository
        self.state = .idle
    }

    func setUnit(with unit: Unit) {
        settingsRepository.setUnits(with: unit)
    }

    func getUnit() {
        settingsRepository.getUnit()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print($0) }) { (unit) in
                self.data = Settings(unit: unit.rawValue)
                self.isMetric = unit == Unit.Metric
                print("<< WE GOT THE UNIT: \(unit.rawValue)")
        }.store(in: &subscriptions)
    }
}
