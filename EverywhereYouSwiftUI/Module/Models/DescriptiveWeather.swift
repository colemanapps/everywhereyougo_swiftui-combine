//
//  DescriptiveWeather.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct DescriptiveWeather: Codable {

    let weatherDescription: String
    let icon: String

    enum CodingKeys: CodingKey { case description, icon }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(weatherDescription, forKey: .description)
        try container.encode(icon, forKey: .icon)
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        try weatherDescription = container.decode(String.self, forKey: .description)
        try icon = container.decode(String.self, forKey: .icon)
    }

    init(weatherDescription: String, icon: String) {
        self.weatherDescription = weatherDescription
        self.icon = icon
    }
}

extension DescriptiveWeather: Persistable {

    public init(managedObject: DescriptiveWeatherObject) {
        weatherDescription     = managedObject.weatherDescription
        icon                   = managedObject.icon
    }

    var managedObject: DescriptiveWeatherObject {
        let descriptiveWeatherObj = DescriptiveWeatherObject()

        descriptiveWeatherObj.weatherDescription = weatherDescription
        descriptiveWeatherObj.icon               = icon

        return descriptiveWeatherObj
    }
}
