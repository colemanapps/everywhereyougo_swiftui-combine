//
//  Main.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct Main: Codable {

    enum CodingKeys: CodingKey { case temp, humidity, temp_min, temp_max }

    let temp: Double
    let humidity: Int
    let temp_min: Double
    let temp_max: Double

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(temp, forKey: .temp)
        try container.encode(temp_min, forKey: .temp_min)
        try container.encode(temp_max, forKey: .temp_max)
        try container.encode(humidity, forKey: .humidity)
    }
}
