//
//  Settings.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import UIKit

enum Unit: String {
    case Imperial = "imperial"
    case Metric = "metric"
}

struct Settings: Codable {
    var unit: String
}

struct Constants {
    static let CORNER_RADIUS: CGFloat = 12
    static let SAVED_UNITS_KEY: String = "SavedUnits"
}
