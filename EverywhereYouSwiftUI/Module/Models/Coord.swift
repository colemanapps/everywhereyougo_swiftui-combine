//
//  Coord.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct Coord: Codable, Equatable {

    let lat: Double
    let lon: Double

    enum CodingKeys: CodingKey { case lat, lon }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(lat, forKey: .lat)
        try container.encode(lon, forKey: .lon)
    }
}

extension Coord: Persistable {
    public init(managedObject: CoordObject) {
        lat = managedObject.lat
        lon = managedObject.lon
    }

    var managedObject: CoordObject {
        let coordObj = CoordObject()

        coordObj.lat = lat
        coordObj.lon = lon

        return coordObj
    }

}
