//
//  Weather.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct Weather: Decodable {

    let temp: Double
    let humidity: Double
    let temp_max: Double
    let temp_min: Double
}
