//
//  CityWeatherReport.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import RealmSwift

struct CityWeatherReport: Codable, Identifiable {

    var id: String { name }
    let weather: [DescriptiveWeather]
    let name: String
    let coordinate: Coord
//    let rain: Rain?
//    let wind: Wind?
//    let main: Main
//    let forecast: [CityWeatherReport]?
//    let fetchTime: Date
//    let dt_txt: String?

    enum CodingKeys: CodingKey { case weather, rain, wind, name, main, coord, fetchTime, forecast, dt_txt }

    init(from decoder: Decoder) throws {
        let weatherContainer = try decoder.container(keyedBy: CodingKeys.self)
        try weather = weatherContainer.decode([DescriptiveWeather].self, forKey: .weather)
        try name =  weatherContainer.decodeIfPresent(String.self, forKey: .name) ?? "No name found"
        try coordinate = weatherContainer.decodeIfPresent(Coord.self, forKey: .coord) ?? Coord(lat: 0, lon: 0)
//        try main = weatherContainer.decode(Main.self, forKey: .main)
//        try wind = weatherContainer.decodeIfPresent(Wind.self, forKey: .wind)
//        try rain = weatherContainer.decodeIfPresent(Rain.self, forKey: .rain)
//        try forecast = weatherContainer.decodeIfPresent([CityWeatherReport].self, forKey: .forecast)
//        try dt_txt =  weatherContainer.decodeIfPresent(String.self, forKey: .dt_txt)

        // needed to refresh data if out of date
//        try fetchTime = weatherContainer.decodeIfPresent(Date.self, forKey: .fetchTime) ?? Date()
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(weather, forKey: .weather)
        try container.encode(name, forKey: .name)
        try container.encode(coordinate, forKey: .coord)
//        try container.encodeIfPresent(wind, forKey: .wind)
//        try container.encodeIfPresent(rain, forKey: .rain)
//        try container.encode(main, forKey: .main)
//        try container.encode(fetchTime, forKey: .fetchTime)
//        try container.encode(forecast, forKey: .forecast)
//        try container.encode(dt_txt, forKey: .dt_txt)
    }

    init(weather: [DescriptiveWeather], name: String, coordinate: Coord) {
        self.weather = weather
        self.name = name
        self.coordinate = coordinate
    }
}

extension CityWeatherReport: Persistable {
    public init(managedObject: CityWeatherReportObject) {
        weather     = managedObject.weather.compactMap(DescriptiveWeather.init(managedObject:))
        name        = managedObject.name
        coordinate  = Coord(managedObject: managedObject.coordinate!)
//        rain        = managedObject.rain
//        wind        = managedObject.wind
//        main        = managedObject.main
//        forecast    = managedObject.forecast
//        fetchTime   = managedObject.fetchTime
//        dt_txt      = managedObject.dt_txt

    }

    var managedObject: CityWeatherReportObject {
        let cityWeatherReportObject = CityWeatherReportObject()

        let weatherListItems = List<DescriptiveWeatherObject>()
        let weatherItems = weather.compactMap { $0.managedObject }
        weatherListItems.append(objectsIn: weatherItems)

        cityWeatherReportObject.weather       = weatherListItems
        cityWeatherReportObject.name          = name
        cityWeatherReportObject.coordinate    = coordinate.managedObject
//        cityWeatherReport.rain          = rain
//        cityWeatherReport.wind          = wind
//        cityWeatherReport.main          = main
//        cityWeatherReport.forecast      = forecast
//        cityWeatherReport.fetchTime     = fetchTime
//        cityWeatherReport.dt_txt        = dt_txt

        return cityWeatherReportObject
    }
}
