//
//  WeatherService.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 24/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import Combine

protocol WeatherServiceContract {
    func fetchWeatherReport(with coordinate: Coord) -> AnyPublisher<CityWeatherReport, Error>
//    func fetchFiveDayForecast(with coordinate: Coord) -> AnyPublisher<Forecast, Error>
}

struct WeatherService: WeatherServiceContract {

    private var subscriptions = Set<AnyCancellable>()

    func fetchWeatherReport(with coordinate: Coord) -> AnyPublisher<CityWeatherReport, Error> {

        let request = getURLRequest(for: coordinate, with: 10)

        return API().perform(request: request)
            .retry(1)
            .decode(type: CityWeatherReport.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }

//    func fetchFiveDayForecast(with coordinate: Coord) -> AnyPublisher<Forecast, Error> {
//
//        let request = getURLRequest(for: coordinate, with: 10)
//
//        return API().perform(request: request)
//            .retry(1)
//            .decode(type: Forecast.self, decoder: JSONDecoder())
//            .eraseToAnyPublisher()
//
//    }
}

extension WeatherService {
    private func getURLRequest(for coordinate: Coord, with timeoutInterval: TimeInterval) -> URLRequest {

        let urlString = URLBuilder.buildFetchWeatherURL(with: coordinate)
        let url = URL(string: urlString)

        guard let urlForRequest = url else {
            fatalError("FATAL ERROR: Creating url from urlstring failed in \(self)")
        }

        return URLRequest(url: urlForRequest, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: timeoutInterval)
    }
}
