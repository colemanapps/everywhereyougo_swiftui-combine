//
//  API.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import Combine

struct API {

    var session: URLSession!

    init(with sessionConfig: URLSessionConfiguration = URLSessionConfiguration.default) {
        sessionConfig.timeoutIntervalForRequest = 10.0
        sessionConfig.timeoutIntervalForResource = 30.0
        session = URLSession(configuration: sessionConfig)
    }

    func perform(request: URLRequest) -> AnyPublisher<Data, URLError> {

        return session
                .dataTaskPublisher(for: request)
                .map(\.data)
                .eraseToAnyPublisher()
    }
}
