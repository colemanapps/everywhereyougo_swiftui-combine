//
//  SettingsDataSource.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 28/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import Combine

struct SettingsLocalDataSource {
    init() {}

    enum DiscError: Error {
        case fetchFromDiscFailed
    }

    func setUnits(with unit: Unit) {
        UserDefaults.standard.set(unit.rawValue, forKey: Constants.SAVED_UNITS_KEY)
    }

    func getUnit() -> Future<Unit, Never> {

        return Future<Unit, Never> { promise in
            if let savedUnits = UserDefaults.standard.string(forKey: Constants.SAVED_UNITS_KEY), let unit = Unit(rawValue: savedUnits) {
                promise(.success(unit))
            } else {
                // if it fails, return Metric as a default
                promise(.success(Unit.Metric))
            }
        }
    }
}

struct SettingsRemoteDataSource {}
