//
//  WeatherDataSource.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import Combine
import RealmSwift

struct WeatherRemoteDataSource {

    private var weatherService: WeatherService

    init(with service: WeatherService) {
        self.weatherService = service
    }

    func addWeatherReport(with coordinate: Coord) -> AnyPublisher<CityWeatherReport, Error> {
        return weatherService.fetchWeatherReport(with: coordinate)
    }

//    func addFiveDayForecast(with coordinate: Coord) -> AnyPublisher<Forecast, Error> {
//       return weatherService.fetchFiveDayForecast(with: coordinate)
//    }
}

struct WeatherLocalDataSource {

    init() {}

    func addWeatherReport(weatherReport: CityWeatherReport) -> Future<CityWeatherReport, Error> {

        return Future<CityWeatherReport, Error> { promise in
            do {
                let realm = try Realm()
                try realm.write {
                    realm.add(weatherReport.managedObject, update: .all)
                    promise(.success(weatherReport))
                }
            } catch {
                promise(.failure(error))
            }
        }
    }

    func getWeatherReports(by names: [String] = []) -> Future<[CityWeatherReport], Error> {

        return Future<[CityWeatherReport], Error> { promise in
            do {
                let realm = try Realm()
                var objects = realm.objects(CityWeatherReportObject.self)
                if !names.isEmpty {
                    objects = objects.filter(NSPredicate(format: "@'name' IN %@", names))
                }

                let weatherReports = objects.compactMap{ $0.model }
                promise(.success(Array(weatherReports)))

            } catch {
                promise(.failure(error))
            }
        }
    }

    func getAllWeatherReports() -> Future<[CityWeatherReport], Error> {
        return getWeatherReports()
    }

    func deleteWeatherReports(reports: [CityWeatherReport]) -> Future<[CityWeatherReport], Error> {

        var reportNames: [String] = []
        reports.forEach { (report) in
            reportNames.append(report.name)
        }

        return Future<[CityWeatherReport], Error> { promise in
            do {

                let realm = try Realm()
                let objectsToDelete = realm.objects(CityWeatherReportObject.self).filter { reportNames.contains($0.name) }
                try realm.write {
                    realm.delete(objectsToDelete)
                }

                let objects = realm.objects(CityWeatherReportObject.self)
                let weatherReports = objects.compactMap{ $0.model }
                promise(.success(Array(weatherReports)))

            } catch {
                promise(.failure(error))
            }
        }
    }
    

//    func addFiveDayForecast(with coordinate: Coord) -> AnyPublisher<Forecast, Error> {
//
//        return Just(Forecast())
//
//    }
}
