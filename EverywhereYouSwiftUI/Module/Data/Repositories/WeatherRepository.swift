//
//  User.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import Combine

struct WeatherRepository: Repository {

    typealias RemoteSource = WeatherRemoteDataSource
    typealias LocalSource = WeatherLocalDataSource

    var remoteDataSource: WeatherRemoteDataSource?
    var localDataSource: WeatherLocalDataSource!

    init(remoteDataSource: WeatherRemoteDataSource?, localDataSource: WeatherLocalDataSource) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }

    func addWeatherReport(with coordinate: Coord) -> AnyPublisher<CityWeatherReport, Error> {

        return remoteDataSource!
            .addWeatherReport(with: coordinate)
            .flatMap { report in
                self.localDataSource.addWeatherReport(weatherReport: report)
        }
        .eraseToAnyPublisher()
    }

    func getAllWeatherReports() -> AnyPublisher<[CityWeatherReport], Error> {

        // there is no remote function for this, so use local always. If this changes, change according to cache policy
        return localDataSource
            .getAllWeatherReports()
            .eraseToAnyPublisher()
    }

    func deleteWeatherReports(reports: [CityWeatherReport]) -> AnyPublisher<[CityWeatherReport], Error> {

        // there is no remote function for this, so use local always. If this changes, change according to cache policy
        return localDataSource
            .deleteWeatherReports(reports: reports)
            .eraseToAnyPublisher()
    }
}
