//
//  SettingsRepository.swift
//  EverywhereYouSwiftUI
//
//  Created by George Coleman on 28/04/2020.
//  Copyright © 2020 Coleman Apps. All rights reserved.
//

import Foundation
import Combine

class SettingsRepository: Repository {

    typealias RemoteSource = SettingsRemoteDataSource
    typealias LocalSource = SettingsLocalDataSource

    var remoteDataSource: SettingsRemoteDataSource?
    var localDataSource: SettingsLocalDataSource!

    required init(remoteDataSource: SettingsRemoteDataSource? = nil, localDataSource: SettingsLocalDataSource) {
        self.localDataSource = localDataSource
    }

    func setUnits(with unit: Unit) {
        localDataSource.setUnits(with: unit)
    }

    func getUnit() -> AnyPublisher<Unit, Never> {
        return localDataSource
            .getUnit()
            .assertNoFailure()
            .eraseToAnyPublisher()
    }
}
